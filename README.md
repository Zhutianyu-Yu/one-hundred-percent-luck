## the README file contains a guide to the contents of the repository.
### the structure of the repository
The repository contains three folders which include a poster, a website (the promotional material) and a prototype (the xd file) respectively.
- Link to the poster: https://gitlab.com/Zhutianyu-Yu/one-hundred-percent-luck/-/tree/master/Poster
- Link to the website: https://gitlab.com/Zhutianyu-Yu/one-hundred-percent-luck/-/tree/master/Promotional%20Materials
- link to the prototype: https://gitlab.com/Zhutianyu-Yu/one-hundred-percent-luck/-/tree/master/Prototype

Also, the documentation can be accessed via the Wiki
- Link to the Wiki: https://gitlab.com/Zhutianyu-Yu/one-hundred-percent-luck/-/wikis/Documentation
### how to use the prototype
The prototype can be accessed under the prototype folder. It is an xd file and can be opened using Adobe XD. There are no login credentials.