$(document).ready(function() {

    $(window).scroll(function() {
        onScroll();
    });

    $(".list-group-item").click(function() {
        if ($(this).attr("id") == "item0") {
            $("li").removeClass("active");
            $(this).addClass("active");
            $(".image-list img:first-of-type").removeClass("display-none");
            $(".image-list img:nth-of-type(2)").addClass("display-none");
            $(".image-list img:nth-of-type(3)").addClass("display-none");
            $(".image-list img:nth-of-type(4)").addClass("display-none");
            $(".image-list img:nth-of-type(5)").addClass("display-none");
            $(".image-list img:nth-of-type(6)").addClass("display-none");
            $(".image-list img:nth-of-type(7)").addClass("display-none");
        }
        else if ($(this).attr("id") == "item1") {
            $("li").removeClass("active");
            $(this).addClass("active");
            $(".image-list img:first-of-type").addClass("display-none");
            $(".image-list img:nth-of-type(2)").removeClass("display-none");
            $(".image-list img:nth-of-type(3)").addClass("display-none");
            $(".image-list img:nth-of-type(4)").addClass("display-none");
            $(".image-list img:nth-of-type(5)").addClass("display-none");
            $(".image-list img:nth-of-type(6)").addClass("display-none");
            $(".image-list img:nth-of-type(7)").addClass("display-none");
        }
        else if ($(this).attr("id") == "item2") {
            $("li").removeClass("active");
            $(this).addClass("active");
            $(".image-list img:first-of-type").addClass("display-none");
            $(".image-list img:nth-of-type(2)").addClass("display-none");
            $(".image-list img:nth-of-type(3)").removeClass("display-none");
            $(".image-list img:nth-of-type(4)").addClass("display-none");
            $(".image-list img:nth-of-type(5)").addClass("display-none");
            $(".image-list img:nth-of-type(6)").addClass("display-none");
            $(".image-list img:nth-of-type(7)").addClass("display-none");
        }
        else if ($(this).attr("id") == "item3") {
            $("li").removeClass("active");
            $(this).addClass("active");
            $(".image-list img:first-of-type").addClass("display-none");
            $(".image-list img:nth-of-type(2)").addClass("display-none");
            $(".image-list img:nth-of-type(3)").addClass("display-none");
            $(".image-list img:nth-of-type(4)").removeClass("display-none");
            $(".image-list img:nth-of-type(5)").addClass("display-none");
            $(".image-list img:nth-of-type(6)").addClass("display-none");
            $(".image-list img:nth-of-type(7)").addClass("display-none");
        }
        else if ($(this).attr("id") == "item4") {
            $("li").removeClass("active");
            $(this).addClass("active");
            $(".image-list img:first-of-type").addClass("display-none");
            $(".image-list img:nth-of-type(2)").addClass("display-none");
            $(".image-list img:nth-of-type(3)").addClass("display-none");
            $(".image-list img:nth-of-type(4)").addClass("display-none");
            $(".image-list img:nth-of-type(5)").removeClass("display-none");
            $(".image-list img:nth-of-type(6)").addClass("display-none");
            $(".image-list img:nth-of-type(7)").addClass("display-none");
        }
    });

    // update the active state of the nav
    function onScroll() {
        // current state
        var scrollPosition = $(document).scrollTop();
        var prototypeSectionDistance = $("#PROTOTYPE").position().top - 20;
        var posterSectionDistance = $("#POSTER").position().top - 20;

        if (scrollPosition >= prototypeSectionDistance && scrollPosition <= posterSectionDistance) {
            $("#prototype").addClass("active-state");
            $("#poster").removeClass("active-state");
        }
        else if (scrollPosition >= posterSectionDistance) {
            $("#prototype").removeClass("active-state");
            $("#poster").addClass("active-state");
        }
        else {
            $("#prototype").removeClass("active-state");
            $("#poster").removeClass("active-state");
        }
    }
});